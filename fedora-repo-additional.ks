# Include the appropriate repo definitions

#  copr for jupytr 
repo --name="orion copr " --baseurl=https://copr-be.cloud.fedoraproject.org/results/orion/jupyter/fedora-$releasever-$basearch/ --cost=1000

# suse repo for retroshare
repo --name=" private for retroshare " --baseurl=http://download.opensuse.org/repositories/home:/AsamK:/RetroShare/Fedora_24/ --cost=1000

# copr for syncthing
repo --name="decathorpe copr"  --baseurl=https://copr-be.cloud.fedoraproject.org/results/decathorpe/syncthing/fedora-$releasever-$basearch/
