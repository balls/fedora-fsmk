#!bin/bash


echo "fastestmirror=true" >> /etc/dnf/dnf.conf
dnf install -y screen


dnf update -y && dnf install -y lorax* virt-install libvirt-daemon-config-network pykickstart anaconda @anaconda-tools screen git emacs mosh htop
setenforce 0

mkdir -p ~/ks ~/logs ~/iso && cd ~/ks
git clone https://pagure.io/forks/deadrat/fedora-kickstarts.git && cd fedora-kickstarts

ksflatten -v, --config fedora-live-fsmk.ks -o flat-fedora-live-fsmk.ks --version F24
cp flat-fedora-live-fsmk.ks ~/logs/ && cd ~/logs/

livemedia-creator --ks flat-fedora-live-fsmk.ks --no-virt --resultdir /var/lmc --project fedora-FSFTN-Live --make-iso --volid fedora-FSFTN-f24 --iso-only --iso-name fedora-FSFTN-f24.iso --releasever 24 --title FSFTN_fedora_custom --nomacboot






