#
# Description:
# - packages for fsmk & fsftn
#
# spam@deadrat.in
# 



%packages
-transmission
@admin-tools
acpi
arduino
arduino-core
asciinema
autoconf 
autogen 
automake
bitlbee
blender
cheese
chromium
emacs
fpaste
firefox
fritzing
gcc 
gcc-c++
geany
gimp
git
gnome-software
inkscape
kicad
mariadb
mariadb-server
meld
mongodb
mongodb-server
nginx
nodejs
os-prober 
parole
pidgin
powertop
pymongo
python-devel
python-flask
python-flask-debugtoolbar
python-flask-login
python-flask-sqlalchemy
python-pip
python-virtualenv
qbittorrent
screen
SDL
sqlite
tor
unzip
uwsgi
vim-common
vim-enhanced
weechat 
wget
youtube-dl 
zlib-devel





#from rpmfusion
rpmfusion-free-release
#rpmfusion-nonfree-release
vlc
#smplayer



#media #includes packages from rpmfusion
gstreamer1
gstreamer1-plugins-base
gstreamer1-plugins-good
#gstreamer1-plugins-ugly
gstreamer1-plugins-bad-free
#gstreamer1-plugins-bad-freeworld
gstreamer1-plugins-good-extras
#gstreamer1-plugins-bad-free-extras

#Jupyter from copr
python3-jupyter_core
python-nbformat

#Overloading
stellarium
celestia
planets
messiggy

#Mesh Stuff:
syncthing
retroshare
vsftpd
filezilla


#just a barebone additional DE
@xfce-desktop

%end
