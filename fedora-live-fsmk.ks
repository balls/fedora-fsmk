#
# Description:
# - Base for fsmk & fsftn
#
# spam@deadrat.in
# 



%include fedora-live-base.ks
%include fedora-rpmfusion-free.ks
%include fedora-rpmfusion-nonfree.ks
%include fedora-fsmk-packages.ks
%include fedora-i3-packages.ks


# incluse a DE # using cinnamon for now
%include fedora-cinnamon-packages.ks




#over-riding locale settings
lang en_GB.UTF-8
keyboard us
timezone Asia/Kolkata

part / --size 8192

%post




#Jupyter notebook
pip3 install --upgrade pip


#test downloading file 
#file in /var/test/


mkdir -p /Files && curl -o /Files/FSFTN_Logo.png https://upload.wikimedia.org/wikipedia/commons/thumb/4/46/FSFTN_Logo.svg/1280px-FSFTN_Logo.svg.png 

curl -o /Files/enigmail.xpi https://www.enigmail.net/download/release/1.9/enigmail-1.9.5-sm+tb.xpi

curl -o /Files/ublock_origin.xpi https://addons.mozilla.org/firefox/downloads/file/509497/ublock_origin-1.9.11rc1-sm+tb+an+fx.xpi


#replace below config based on DE




# cinnamon configuration

# create /etc/sysconfig/desktop (needed for installation)

cat > /etc/sysconfig/desktop <<EOF
PREFERRED=/usr/bin/cinnamon-session
DISPLAYMANAGER=/usr/sbin/lightdm
EOF

cat >> /etc/rc.d/init.d/livesys << EOF

# set up lightdm autologin
sed -i 's/^#autologin-user=.*/autologin-user=liveuser/' /etc/lightdm/lightdm.conf
sed -i 's/^#autologin-user-timeout=.*/autologin-user-timeout=0/' /etc/lightdm/lightdm.conf
#sed -i 's/^#show-language-selector=.*/show-language-selector=true/' /etc/lightdm/lightdm-gtk-greeter.conf

# set Cinnamon as default session, otherwise login will fail
sed -i 's/^#user-session=.*/user-session=cinnamon/' /etc/lightdm/lightdm.conf

# Show harddisk install on the desktop
sed -i -e 's/NoDisplay=true/NoDisplay=false/' /usr/share/applications/liveinst.desktop
mkdir /home/liveuser/Desktop
cp /usr/share/applications/liveinst.desktop /home/liveuser/Desktop

# and mark it as executable
chmod +x /home/liveuser/Desktop/liveinst.desktop

# this goes at the end after all other changes. 
chown -R liveuser:liveuser /home/liveuser
restorecon -R /home/liveuser

EOF

%end
